package ru.news.util;


import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;

public class ViewUtil {
    public static void visibleOrGone(@NonNull View view, boolean visible) {
        view.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    public static void visibleOrInvisible(@NonNull View view, boolean visible) {
        view.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
    }

    public static boolean isVisible(@NonNull View view) {
        return view.getVisibility() == View.VISIBLE;
    }

    public static void picassoLoadImage(@NonNull String url, @NonNull ImageView imageView) {
        Picasso.with(imageView.getContext())
                .load(url)
                .fit()
                .centerCrop()
                .into(imageView);
    }

    public static void picassoLoadImage(@NonNull File file, @NonNull ImageView imageView) {
        Picasso.with(imageView.getContext())
                .load(file)
                .fit()
                .centerCrop()
                .into(imageView);
    }

    public static void picassoLoadImageNoCache(@NonNull File file, @NonNull ImageView imageView) {
        Picasso.with(imageView.getContext())
                .load(file)
                .fit()
                .centerCrop()
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                .into(imageView);
    }
    public  static String getHtmlTextWithFont(String htmlText, String font, String backgroundColor) {
        return "<html>\n" +
                "<head>\n" +
                "<title></title>\n" +
                "<style type=\"text/css\">\n" +
                "body {\n" +
                "    background-color: " + backgroundColor + ";\n" +
                "}\n" +
                " \n" +
                "@font-face {\n" +
                "    font-family: roboto;\n" +
                "    src: url('file:///android_asset/" + font + "');\n" +
                "}\n" +
                " \n" +
                "p {\n" +
                "    color: #000000;\n" +
                "    font-family: roboto, Verdana, sans-serif;\n" +
                "}\n" +
                "img{\n" +
                "display: inline; height: auto; max-width: 100%;\n" +
                "}\n" +
                "</style>\n" +
                "</head>\n" +
                "<body>\n" +
                "    <p style=\"font-family: roboto\">" + htmlText + "</p>\n" +
                " \n" +
                "</body>\n" +
                "</html>";
    }
}
