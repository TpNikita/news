package ru.news.screen;


import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.EditText;
import android.widget.SearchView;


import com.arellomobile.mvp.InjectViewState;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.news.base.presenter.BasePresenter;
import ru.news.domain.news.NewsUseCase;
import ru.news.domain.news.SearchNewsUseCase;

@InjectViewState
public class MainPresenter extends BasePresenter<MainView> {

    private final String TAG = this.getClass().getSimpleName();

    private final NewsUseCase newsUseCase = new NewsUseCase();
    private final SearchNewsUseCase searchNewsUseCase = new SearchNewsUseCase();

    public MainPresenter() {
        loadNews();
    }

    public void loadNews() {
        addDisposable(newsUseCase.run(new NewsUseCase.Params())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable -> getViewState().showLoading(true))
                .doOnTerminate(() -> getViewState().showLoading(false))
                .onTerminateDetach()
                .subscribe(result -> {
                    getViewState().showNews(result.getNewsModelCompact());
                    Log.i(TAG, "Download news");
                }, throwable -> {
                    getViewState().showErrorDialog();
                    Log.e(TAG, "Error load news");
                }));
    }

    public void setObs(@NonNull SearchView searchView) {
        addDisposable(
                searchNewsUseCase.run(new SearchNewsUseCase.Params(searchView))
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(disposable -> getViewState().showLoading(true))
                        .doOnTerminate(() -> getViewState().showLoading(false))
                        .onTerminateDetach()
                        .subscribe(result -> {
                            getViewState().showNews(result.getNewsModelCompact());
                            Log.i(TAG, "Download news");
                        }, throwable -> {
                           getViewState().showErrorDialog();
                            Log.e(TAG, "Error load news");
                        })

        );
    }
}
