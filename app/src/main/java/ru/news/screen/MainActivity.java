package ru.news.screen;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.news.R;
import ru.news.base.BaseActivity;
import ru.news.domain.news.NewsUseCase;

public class MainActivity extends BaseActivity {

    public static void start(@NonNull Context context) {
        Intent starter = new Intent(context, MainActivity.class);
        context.startActivity(starter);
    }

    @Nullable
    @Override
    protected Fragment getContentFragment() {
        return MainFragment.newInstance();
    }


}
