package ru.news.screen;


import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ru.news.InternetStatusReceiver;
import ru.news.R;
import ru.news.base.BaseFragment;
import ru.news.base.view.DialogLoadingViewDelegate;
import ru.news.domain.model.NewsModelCompact;

public class MainFragment extends BaseFragment implements MainView, MenuItemCompat.OnActionExpandListener {

    private final String TAG = this.getClass().getSimpleName();

    private boolean internetStatus = true;

    private Unbinder unbinder;

    private InternetStatusReceiver internetStatusReceiver;

    NewsListAdapter adapter;

    @InjectPresenter
    MainPresenter presenter;

    private MenuItem searchMenuItem;
    private SearchView mSearchView;

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    private DialogLoadingViewDelegate dialogLoadingViewDelegate;

    public static MainFragment newInstance() {
        Bundle args = new Bundle();
        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    protected Integer getLayoutId() {
        return R.layout.main;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        toolbar.setNavigationOnClickListener(v -> closeView());
        dialogLoadingViewDelegate = new DialogLoadingViewDelegate(getContext());
        register();
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.news);
        adapter = new NewsListAdapter();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext());
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(itemAnimator);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        setHasOptionsMenu(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.toolbar_menu, menu);
        searchMenuItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) searchMenuItem.getActionView();
        presenter.setObs(mSearchView);
        MenuItemCompat.setOnActionExpandListener(searchMenuItem, this);
        super.onCreateOptionsMenu(menu, inflater);

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        dialogLoadingViewDelegate.destroy();
    }

    @Override
    public void showLoading(boolean loading) {
        dialogLoadingViewDelegate.showLoading(loading);
    }

    public void register() {
        internetStatusReceiver = new InternetStatusReceiver() {
            public void onReceive(Context context, Intent intent) {
                if (!isNetworkAvailable() && internetStatus) {
                    internetStatus = false;
                    Toast.makeText(context, getResources().getText(R.string.main_lost_connection_internet), Toast.LENGTH_SHORT).show();
                } else if (isNetworkAvailable() && !internetStatus) {
                    internetStatus = true;
                    Toast.makeText(context, getResources().getText(R.string.main_restore_internet_connection), Toast.LENGTH_SHORT).show();
                    presenter.loadNews();

                }
            }
        };
        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        getContext().registerReceiver(internetStatusReceiver, intentFilter);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void showErrorDialog() {
        internetStatus = false;
        Toast.makeText(getContext(), getResources().getText(R.string.main_loading_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showNews(@NonNull List<NewsModelCompact> news) {
        adapter.setData(news);
        Log.i(TAG, "showNews");
    }

    @Override
    public boolean onMenuItemActionExpand(MenuItem item) {
        return true;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem item) {
        presenter.loadNews();
        return true;
    }
}
