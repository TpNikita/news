package ru.news.screen;


import android.support.annotation.NonNull;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;

import ru.news.base.view.CloseableView;
import ru.news.base.view.ErrorView;
import ru.news.base.view.LoadingView;
import ru.news.domain.model.NewsModelCompact;

public interface MainView extends MvpView,LoadingView,CloseableView,ErrorView{

    @StateStrategyType(AddToEndSingleStrategy.class)
    void showNews(@NonNull List<NewsModelCompact> news);
}
