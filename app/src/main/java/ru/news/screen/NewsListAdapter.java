package ru.news.screen;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.news.R;
import ru.news.domain.model.NewsModelCompact;
import ru.news.util.ViewUtil;

public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.NewsViewHolder> {

    private final String TAG = this.getClass().getSimpleName();

    private List<NewsModelCompact> news = Collections.emptyList();

    public void setData(@NonNull List<NewsModelCompact> news) {
        Log.i(TAG, "setData");
        this.news = news;
        for (int i = 0; i < this.news.size(); i++) {
            if ((!URLUtil.isNetworkUrl(this.news.get(i).getImagePreview()) || (this.news.get(i).getBody().length() == 0))) {
                this.news.remove(i);
                i--;
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_list_item, parent, false);
        NewsViewHolder nvh = new NewsViewHolder(v);
        return nvh;
    }

    @Override
    public void onBindViewHolder(NewsViewHolder holder, int i) {
        if (URLUtil.isNetworkUrl(news.get(i).getImagePreview())) {
            ViewUtil.picassoLoadImage(news.get(i).getImagePreview(), holder.news_prewiew);
        }
        holder.content.loadDataWithBaseURL(null, ViewUtil.getHtmlTextWithFont(news.get(i).getBody(), "fonts/Roboto-Regular.ttf", "#ffffff"), "text/html", "utf-8", null);
    }

    @Override
    public int getItemCount() {
        return news.size();
    }

    public static class NewsViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.news_preview)
        protected ImageView news_prewiew;
        @BindView(R.id.content)
        protected WebView content;

        public NewsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
