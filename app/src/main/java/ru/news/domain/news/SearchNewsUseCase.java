package ru.news.domain.news;


import android.support.annotation.NonNull;
import android.widget.EditText;
import android.widget.SearchView;

import com.jakewharton.rxbinding2.widget.RxSearchView;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import ru.news.data.network.RestClient;
import ru.news.domain.UseCase;
import ru.news.domain.mapping.NewsResultListNewsListMapper;
import ru.news.domain.model.NewsModelCompact;

public class SearchNewsUseCase extends UseCase<SearchNewsUseCase.Params, SearchNewsUseCase.Result> {

    private final NewsResultListNewsListMapper newsResultListNewsListMapper = new NewsResultListNewsListMapper();

    @Override
    public Observable<Result> run(Params params) {
        return RxSearchView.queryTextChangeEvents(params.getSearchView())
                .debounce(1500, TimeUnit.MILLISECONDS)
                .filter(searchViewQueryTextEvent -> searchViewQueryTextEvent.queryText().toString().length() > 2)
                .observeOn(Schedulers.io())
                .flatMap(searchViewQueryTextEvent -> RestClient.getInstance().searchNews(searchViewQueryTextEvent.queryText().toString())
                        .map(result -> newsResultListNewsListMapper.map(result))
                        .map(Result::new).toObservable());

    }

    public static class Params extends UseCase.Params {

        private final SearchView searchView;

        public Params(@NonNull SearchView searchView) {
            this.searchView = searchView;
        }

        public SearchView getSearchView() {
            return searchView;
        }
    }

    public static class Result extends UseCase.Result {
        private final List<NewsModelCompact> newsModelCompact;

        public Result(List<NewsModelCompact> newsModelCompact) {
            this.newsModelCompact = newsModelCompact;
        }

        public List<NewsModelCompact> getNewsModelCompact() {
            return newsModelCompact;
        }

    }

}
