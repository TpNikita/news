package ru.news.domain.news;


import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.news.data.network.RestClient;
import ru.news.domain.UseCase;
import ru.news.domain.mapping.NewsResultListNewsListMapper;
import ru.news.domain.model.NewsModelCompact;


public class NewsUseCase extends UseCase<NewsUseCase.Params, NewsUseCase.Result> {

    private final NewsResultListNewsListMapper newsResultListNewsListMapper = new NewsResultListNewsListMapper();

    @Override
    public Observable<Result> run(Params params) {
        return RestClient.getInstance().news()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess(result -> {

                })
                .map(result -> newsResultListNewsListMapper.map(result))
                .map(Result::new)
                .toObservable();

    }

    public static class Params extends UseCase.Params {
    }

    public static class Result extends UseCase.Result {
        private final List<NewsModelCompact> newsModelCompact;

        public Result(List<NewsModelCompact> newsModelCompact) {
            this.newsModelCompact = newsModelCompact;
        }

        public List<NewsModelCompact> getNewsModelCompact() {
            return newsModelCompact;
        }

    }
}
