package ru.news.domain.model;


import java.util.List;

public class NewsModelCompact {

   /* private final Integer id;
    private final String publishDate;
    private final Integer top;
    private final String title;
    private final String subTitle;*/
    private final String body;
    /*private final String note;
    private final String shareLink;
    private final List<String> tags = null;*/
    private final String imagePreview;
    /*private final String imageMain;
    private final String videoStream;
    private final String videoStreamPreview;*/

    public NewsModelCompact(/*Integer id, String publishDate, Integer top, String title,
                            String subTitle, */String body,/* String note, String shareLink,*/ String imagePreview/*, String imageMain, String videoStream, String videoStreamPreview*/) {
   /*     this.id = id;
        this.publishDate = publishDate;
        this.top = top;
        this.title = title;
        this.subTitle = subTitle;*/
        this.body = body;
       /* this.note = note;
        this.shareLink = shareLink;*/
        this.imagePreview = imagePreview;
       /* this.imageMain = imageMain;
        this.videoStream = videoStream;
        this.videoStreamPreview = videoStreamPreview;*/

    }


    /*public Integer getId() {
        return id;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public Integer getTop() {
        return top;
    }

    public String getTitle() {
        return title;
    }

    public String getSubTitle() {
        return subTitle;
    }
*/
    public String getBody() {
        return body;
    }

  /*  public String getNote() {
        return note;
    }

    public String getShareLink() {
        return shareLink;
    }

    public List<String> getTags() {
        return tags;
    }
*/
    public String getImagePreview() {
        return imagePreview;
    }

 /*   public String getImageMain() {
        return imageMain;
    }

    public String getVideoStream() {
        return videoStream;
    }

    public String getVideoStreamPreview() {
        return videoStreamPreview;
    }
*/
}
