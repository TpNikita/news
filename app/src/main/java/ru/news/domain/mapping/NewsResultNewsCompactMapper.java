package ru.news.domain.mapping;


import ru.news.data.network.model.NewsResult;
import ru.news.domain.model.NewsModelCompact;

public class NewsResultNewsCompactMapper extends Mapping<NewsResult, NewsModelCompact> {
    @Override
    public NewsModelCompact map(NewsResult newsResult) {
        return new NewsModelCompact(/*newsResult.getId(), newsResult.getPublishDate(), newsResult.getTop(), newsResult.getTitle()
                , newsResult.getSubTitle(), */newsResult.getBody(),/* newsResult.getNote(), newsResult.getShareLink()
                , */newsResult.getImagePreview()/*, newsResult.getImageMain(), newsResult.getVideoStream(), newsResult.getVideoStreamPreview()*/);
    }
}
