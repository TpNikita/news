package ru.news.domain.mapping;


import java.util.ArrayList;
import java.util.List;

import ru.news.data.network.model.NewsResult;
import ru.news.domain.model.NewsModelCompact;

public class NewsResultListNewsListMapper extends Mapping<List<NewsResult>, List<NewsModelCompact>> {

    private final NewsResultNewsCompactMapper newsResultNewsCompactMapper = new NewsResultNewsCompactMapper();

    @Override
    public List<NewsModelCompact> map(List<NewsResult> newsResults) {
        ArrayList<NewsModelCompact> news = new ArrayList<>(newsResults.size());
        for (NewsResult newsResult : newsResults) {
            news.add(newsResultNewsCompactMapper.map(newsResult));
        }
        return news;
    }
}
