package ru.news.domain;


import io.reactivex.Observable;

public abstract class UseCase<P extends UseCase.Params, R extends UseCase.Result> {

    public abstract Observable<R> run(P params);

    public abstract static class Params {
    }

    public abstract static class Result {
    }
}

