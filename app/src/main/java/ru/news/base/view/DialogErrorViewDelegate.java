package ru.news.base.view;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

public class DialogErrorViewDelegate implements ErrorView {
    private final Context context;
    private AlertDialog.Builder builder;
    AlertDialog alert;

    public DialogErrorViewDelegate(@NonNull Context context) {
        this.context = context;
    }

    public void showErrorDialog() {
    }

    public void destroy() {
        dismissCurrentDialog();
    }

    private void dismissCurrentDialog() {
        if (alert != null) {
            alert.dismiss();
            alert = null;
        }
    }
}
