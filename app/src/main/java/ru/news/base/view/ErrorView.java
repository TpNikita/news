package ru.news.base.view;


import android.support.annotation.NonNull;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

public interface ErrorView {
    @StateStrategyType(AddToEndSingleStrategy.class)
    void showErrorDialog();
}
