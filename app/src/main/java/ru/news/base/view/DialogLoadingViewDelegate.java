package ru.news.base.view;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;

import ru.news.R;


public class DialogLoadingViewDelegate implements LoadingView {

    private final Context context;
    private Dialog dialog;

    public DialogLoadingViewDelegate(@NonNull Context context) {
        this.context = context;
    }

    @SuppressLint("InflateParams")
    @Override
    public void showLoading(boolean loading) {
        if (loading && dialog == null) {
            dialog = new AlertDialog.Builder(context)
                    .setView(LayoutInflater.from(context).inflate(R.layout.dialog_loading, null))
                    .setCancelable(false)
                    .show();
        } else if (!loading && dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }

    public void destroy() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }
}
