package ru.news.base;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.arellomobile.mvp.MvpAppCompatActivity;

import ru.news.base.view.CloseableView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BaseActivity extends MvpAppCompatActivity implements CloseableView{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Integer layout = getLayout();
        if (layout != null) {
            setContentView(layout);
        }
        if (savedInstanceState == null) {
            Fragment fragment = getContentFragment();
            if (fragment != null) {
                replaceContentFragmentWith(fragment);
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Nullable
    @LayoutRes
    protected Integer getLayout() {
        return null;
    }

    @Nullable
    protected Fragment getContentFragment() {
        return null;
    }

    @IdRes
    protected int getIdForContentFragment() {
        return android.R.id.content;
    }

    protected void replaceContentFragmentWith(@NonNull Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(getIdForContentFragment(), fragment)
                .commitNow();
    }

    @Override
    public void closeView() {
        onBackPressed();
    }
}
