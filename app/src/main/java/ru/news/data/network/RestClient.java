package ru.news.data.network;

import java.util.List;

import io.reactivex.Single;
import okhttp3.*;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import ru.news.data.network.model.NewsResult;

public class RestClient {


    private static RestClient instance;

    private ServerAPI serverAPI;

    public static RestClient getInstance() {
        if (instance == null) {
            instance = new RestClient();
        }
        return instance;

    }

    private RestClient() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(ServerAPI.HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
                .validateEagerly(true)
                .build();
        serverAPI = retrofit.create(ServerAPI.class);
    }

    public Single<List<NewsResult>> news() {
        return serverAPI.news();
    }

    public Single<List<NewsResult>> searchNews(String word) {
        return serverAPI.searchNews(word);
    }

}
