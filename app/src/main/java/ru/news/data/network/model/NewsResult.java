package ru.news.data.network.model;


import java.util.List;

import com.google.gson.annotations.SerializedName;

public class NewsResult {
   /* @SerializedName("Id")
    private Integer id;

    @SerializedName("PublishDate")
    private String publishDate;

    @SerializedName("Top")
    private Integer top;

    @SerializedName("Title")
    private String title;

    @SerializedName("SubTitle")
    private String subTitle;*/

    @SerializedName("Body")
    private String body;

    /*@SerializedName("Note")
    private String note;

    @SerializedName("ShareLink")
    private String shareLink;

    @SerializedName("Tags")
    private List<String> tags = null;*/

    @SerializedName("ImagePreview")
    private String imagePreview;

   /* @SerializedName("ImageMain")
    private String imageMain;

    @SerializedName("VideoStream")
    private String videoStream;

    @SerializedName("VideoStreamPreview")
    private String videoStreamPreview;*/

    /*public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public Integer getTop() {
        return top;
    }

    public void setTop(Integer top) {
        this.top = top;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }*/

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
/*
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getShareLink() {
        return shareLink;
    }

    public void setShareLink(String shareLink) {
        this.shareLink = shareLink;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }*/

    public String getImagePreview() {
        return imagePreview;
    }

    public void setImagePreview(String imagePreview) {
        this.imagePreview = imagePreview;
    }
 /*
    public String getImageMain() {
        return imageMain;
    }

    public void setImageMain(String imageMain) {
        this.imageMain = imageMain;
    }

    public String getVideoStream() {
        return videoStream;
    }

    public void setVideoStream(String videoStream) {
        this.videoStream = videoStream;
    }

    public String getVideoStreamPreview() {
        return videoStreamPreview;
    }

    public void setVideoStreamPreview(String videoStreamPreview) {
        this.videoStreamPreview = videoStreamPreview;
    }*/
}
