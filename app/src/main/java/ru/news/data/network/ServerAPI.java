package ru.news.data.network;

import java.util.List;

import retrofit2.*;
import retrofit2.http.GET;
import io.reactivex.Single;
import retrofit2.http.Path;
import retrofit2.http.Query;
import ru.news.data.network.model.NewsResult;

public interface ServerAPI {
    String HOST = "https://fapi.rtvi.com/api/v1/";

    @GET("newslist")
    Single<List<NewsResult>> news();

    @GET("newslist")
    Single<List<NewsResult>> searchNews(@Query("searchString") String word);

}
